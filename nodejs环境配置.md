# nodejs相关环境配置

## 推荐安装nvm

['下载地址'](https://github.com/coreybutler/nvm-windows/releases)

```
$ nvm list [available]
$ nvm install
$ nvm uninstall
$ nvm use
```

## 推荐安装到全局使用npm，安装到项目中使用pnpm，或者yarn，这样不就不用再配置yarn和pnpm的全局安装路径

## 配置npm

```
$ npm config set prefix "D:\Programs\nodejs" # 设置global bin
$ npm config set cache "D:\Programs\nodejs\npm-cache" # 缓存位置
$ npm config get cache # 查看缓存位置
$ npm cache clean --force # 清理缓存
$ npm config set registry http://registry.npmmirror.com # 设置淘宝代理
$ npm config set registry https://registry.npmjs.org # 恢复官方代理
```

## 安装并配置yarn

```
$ npm i yarn -g # 安装yarn
$ yarn config set cache-folder "D:\Programs\nodejs\yarn-cache" # 设置缓存目录
$ yarn cache dir # 查看缓存目录
$ yarn cache clean # 清理缓存
```

## 安装并配置pnpm

```
$ npm i pnpm -g
$ pnpm store path # 查看store位置
$ pnpm store prune # 清理
$ pnpm config set store-dir "D:\.pnpm-store" # 设置store位置
```

## 安装node-gyp(根据个人需要)

```
$ npm install -g node-gyp
```

### 通过npm_config_OPTION_NAME设置比如：

```
$ set npm_config_python=D:\Programs\Python27\python.exe # 使用python2
$ set npm_config_python=D:\Programs\Python310\python.exe # 使用python3
```

`$ set npm_config_devdir=D:\Programs\.gyp ` # 配置devdir

#### npm版本小于v9使用npm config set [--global] OPTION_NAME 的方式设置

```
$ npm config set --global devdir D:/Programs/.gyp
```

### windows-build-tools # (根据个人需要) 提示此包已弃用,现在的nodejs安装时会自动安装环境

#### 以管理员身份安装

```
$ npm install --global --production windows-build-tools # 在线安装
$ npm install -g windows-build-tools --offline-installers="C:\Users\<Me>\.windows-build-tools" # 离线安装
```

#### C:\Users\<Me>\.windows-build-tools\vs_BuildTools.exe python2.7.15 # 下载的安装包保存位置

##### 以windows非管理员下安装，要保证有当前用户权限

```
$ set # 查看当前环境变量
$ set APPDATA=C:\Users\<Me>\AppData\Roaming # 设置环境变量
$ set USERNAME=<Me> # 设置环境变量
$ set USERPROFILE=C:\Users\<Me> # 设置环境变量
$ npm config set prefix C:\Users\<Me>\AppData\Roaming\npm # 安装到这里有用户权限
```
