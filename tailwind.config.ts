
import type { Config } from 'tailwindcss'
// ts项目
export default {
    content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
    darkMode: 'media', // or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {},
    plugins: [],
} satisfies Config
