import type { UserConfig } from '@commitlint/types';
import { RuleConfigSeverity } from '@commitlint/types';

const Configuration: UserConfig = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        'type-enum': [
            RuleConfigSeverity.Error,
            'always',
            [
                'feat',
                'fix',
                'docs',
                'style',
                'refactor',
                'perf',
                'test',
                'revert',
                'ci',
                'chore',
                'workflow',
                'mod',
                'wip',
                'types',
                'release',
            ],
        ],
        'subject-full-stop': [RuleConfigSeverity.Disabled, 'never', ''],
        'subject-case': [RuleConfigSeverity.Disabled, 'never', 'lower-case'],
    },
};
module.exports = Configuration;
