# 管理系统

## 关于本项目
-   基于[pdv-basic] (https://gitee.com/liweisum/pdv-basic)
-   使用了typescript，如果不想使用typescript，请移步[pdv-admin-js项目](https://gitee.com/liweisum/pdv-admin-js)
-   weppack 打包
-   react 技术栈
-   react-router-dom 路由
-   redux + react-redux + redux-thunk 状态管理
-   axios 请求接口
-   antd 组件库
-   tailwindcss 减小css文件大小
-   less 支持less
-   scss 支持scss
-   svg 引用SVG图标
-   按钮换肤
-   暗黑主题
-   支持设置多语
-   mock接口生成菜单
-   鉴权
-   门户页以页签方式打开页面
-   css module 文件名以.module.css、.module.scss 、.module.less 结尾会开启
-   eslint规范
-   stylelint样式规范
-   perttier格式化
-   husky + lint-staged + commitlint git提交规范

## 如何使用

```
  $ git clone https://gitee.com/liweibty/pdv-web-ts.git
  $ cd pdv-web-ts
  $ pnpm i
  $ pnpm start # 本地启动
  $ pnpm run build # 打包
  $ npm install -g serve # 安装serve
  $ serve -s dist # 启动serve预览打包后的dist

```

## 1 前端开发规范

### 1.1 推荐使用typescript

    如果不想使用typescript,建议使用prop-types添加props类型

```
pnpm install --save prop-types
```

### 1.2 使用eslint规范js代码

这里采用了热门的airbnb方案配置

```
npm init -y
npm init @eslint/config
```

### 1.3 使用 perttier 格式化代码

    统一的格式化，避免代码格式化导致提交代码变化太多

### 1.4 使用 stylelint 规范样式

#### 1.4.1 避免样式冲突

##### 1.4.1.1 可使用css-module

    *.module.css

##### 1.4.1.2 classname 使用BEM命名规范

            B代表block
            E代表element
            M代表modifier
            例如：
            B__E--M
            B user-list
            E search-button
            M disabled-dark

            user-list__search-button--disabled-dark

#### 1.4.2 应该避免层级太深

    尽可能的减少元素的嵌套，减少css选择器的层级

### 1.5 使用tailwindcss（可选）

    可以大大减少样式文件的大小

### 1.6 使用Git Hook

    husky

    lint-staged 提交前先对代码进行检查，防止错误的不规范的代码被提交
    commitlint 提交说明内容检查，规范用户提交描述信息

#### 1.6.1 安装husky

```
    $ pnpm add --save-dev husky
    $ pnpm exec husky init #初始化husky
    $ pnpm run prepare #项目准备时执行上面这2行命令
```

    pnpm exec husky init 会在package.json中增加prepare，可用下面命令实现

`$ pnpm pkg set scripts.prepare="husky install"// package.json中script中增加prepare`

#### 1.6.2 安装lint-staged(pre-commit阶段检查代码)

```
    $ pnpm i lint-staged -D # 安装lint-staged
    $ pnpx mrm@2 lint-staged # 集合
```

-   会将下面的命令添加到.husky/pre-commit中,要根据使用的npm，yarn、pnpm 添加
-   如果使用的是 npm/yarn 且 npm 版本为 v8 及以下，就加命令 npx lint-staged
-   如果使用的是 npm/yarn 且 npm 版本为 v9 及以上，就加命令 npm exec lint-staged
-   如果使用的是 pnpm，就加命令 pnpm exec lint-staged
-   也可以自己添加

```
    $ pnpx husky add .husky/pre-commit "pnpm exec lint-staged"
```

#### 1.6.3 安装commitlint(commit-msg阶段，检查提交描述信息)

```
    $ npm install --save-dev @commitlint/config-conventional @commitlint/cli
    $ echo "module.exports = { extends: ['@commitlint/config-conventional'] };" > commitlint.config.js
    $ pnpx husky add .husky/commit-msg  'npx --no -- commitlint --edit ${1}'
```

**常用type**
| type | 标题2 |
|-------|-------|
| feat | 新增功能 |
| fix | bug 修复 |
| style | 不影响程序逻辑的代码修改(修改空白字符，补全缺失的分号等) |
| refactor | 重构代码(既没有新增功能，也没有修复 bug) |
| docs | 文档更新 |
| test | 增加测试 |
| chore | 构建过程或辅助工具的变动 |
_在commitlist.config中查看_

#### 1.6.4 endOfLine配置

-   windows endOfLine格式为crlf
-   mac endOfLine格式为lf
-   git 会自动转换
-   配置统一使用lf

_关闭git自动转换_

```
$ git config --global core.autocrlf false
```

_.editorconfig中配置_

```
end_of_line = lf
```

_.prettierrc中配置_

```
    {
        endOfLine:"lf"
    }
```

_.eslintrc中配置_

```
rules:{
 "linebreak-style": ["error", "unix"],
       "prettier/prettier": [
            "error",
            {
                "endOfLine": "lf"
            }
        ]
}
```

#### 1.6.5 推荐使用pnpm

## 2 优秀组件推荐

### 2.1 react-draggable

    拖拽改变位置

### 2.2 react-intl

    国际化

### 2.3 react-markdown

    渲染markdown

### 2.4 dnd-kit

    列表拖拽排序

### 2.5 classnames

    简化classname判断逻辑

### 2.6 lodash-es

    数据深拷贝等

### 2.7 react-resizable

    拖拽改变大小

### 2.8 ckeditor

    富文本编辑器

## 3 todo

_是否可以做成动态可配置_
| 序号 | 描述 |
|-------|-------|
| 3.1 | 是否需要多语 |
| 3.2 | 是否需要门户页 |
| 3.3 | 是否需要面板页签 |
| 3.4 | 是否需要侧边栏 |
| 3.5 | 是否需要登录鉴权 |
| 3.6 | 是否需要tyscript |
| 3.7 | 是否需要redux |
| 3.8 | 是否需要less |
| 3.9 | 是否需要 scss |
| 3.10 | 是否需要antd |
| 3.11 | 是否需要切换主题 |
| 3.12 | 是否使用前端微服务 |
| 3.13 | 是否webpack还是vite |
| 3.14 | 多皮肤背景图 |
| 3.15 | 可根据配置不再引入相应的组件 |
| 3.16 | 单应用、多应用 |
| 3.17 | 单点登录 |
| 3.18 | websocket |
| 3.19 | sse |
| 3.20 |大模型 |
| 3.21 | 视频、语音 |
| 3.22 | canvas |
| 3.23 | three 3D |
| 3.24 | 大屏 |
