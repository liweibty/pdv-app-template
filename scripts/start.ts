// 采用node api的方式启动
import Webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import configFactory from '../config/webpack.config';

process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

const webpackConfig = configFactory({
    development: true
}, {})
const { devServer, ...config } = webpackConfig
const compiler = Webpack(config);
const devServerOptions = { ...devServer };
const server = new WebpackDevServer(devServerOptions, compiler);
// 打开成功
server.startCallback(() => {
    console.log('Successfully started server on http://localhost:3000');
});
server.stopCallback(() => {
    console.log('Server stopped.');
});
async function runServer() {
    console.log('Starting server...');
    await server.start();
};

async function stopServer() {
    console.log('Stopping server...');
    await server.stop();
};
// 这里不用调用start了，devServer中配置了open:true浏览器已经打开了
// runServer();
