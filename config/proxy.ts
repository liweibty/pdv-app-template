import { ProxyConfigArray } from 'webpack-dev-server'

const proxy: ProxyConfigArray = [
    {
        context: (param: string) =>
            !!param.match(
                /\/api\/.*|git\/.*|opencv\/.*|analytics\/.*|static\/.*|admin(?:\/(.*))?.*|documentation\/.*|django-rq(?:\/(.*))?/gm,
            ),
        target: 'http://localhost:7000',
        secure: false,
        changeOrigin: true,
    },
];

export default proxy;
