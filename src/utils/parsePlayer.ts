/**
 * 用于将markdown中插入的dplayer视频转为html
 * @param text markdown编辑的内容
 * @returns
 */
export default function parsePlayer(text: string) {
    const reg = /(\[dplayer).*?(\/\])/gi;
    return text.replace(reg, (str: string) => {
        const attrs: { [key: string]: any } = {};
        const tags = str.match(/[\S]+/g);
        tags?.forEach((item: string) => {
            // 获取参数
            if (item.includes('=')) {
                const [key, value] = item.split('=');
                const res = JSON.parse(value);
                if (key === 'autoplay') {
                    attrs[key] = JSON.parse(res);
                }
                attrs[key] = res;
            }
        });
        return `<div class="dplayer" data-config=${JSON.stringify(attrs)}></div>`;
    });
}
