import React, { lazy, Suspense } from 'react';
import { createHashRouter, createBrowserRouter, Outlet } from 'react-router-dom';

// component
import RootErrorBoundary from '@/components/root-error-boundary'; // root错误边界
import { RequireAuth } from '@/components/auth'; // 需要登录权限
import { NavProvider } from '@/components/nav'; // 门户页的导航，会自动添加面板页签
// page
import { Project, projectLoader, ProjectErrorBoundary } from '@/containers/project-page';
import LoginPage from '@/containers/login-page'; // 登录页作为起始页，不懒加载
import HomePage from '@/containers/home-page';

// lazy page
const RegisterPage = lazy(() => import('@/containers/register-page')); // 注册
const LayoutPage = lazy(() => import('@/containers/layout-page')); // 门户页
const NoMatchPage = lazy(() => import('@/containers/no-match-page')); // 404页面
const DashboardPage = lazy(() => import('@/containers/dashboard-page'));
const InfiniteScrollBottomPage = lazy(() => import('@/containers/infinite-scroll-bottom-page'));
const InfiniteScrollTopPage = lazy(() => import('@/containers/infinite-scroll-top-page'));
const VirtualListPage = lazy(() => import('@/containers/virtual-list-page'));
const ProtectedPage = lazy(() => import('@/containers/protected-page')); //
const DisplayPage = lazy(() => import('@/containers/display-page')); //
const ShopPage = lazy(() => import('@/containers/shop-page')); //
const GamePage = lazy(() => import('@/containers/game-page')); //
const LivePage = lazy(() => import('@/containers/live-page')); //
const PdfViewEmbedPage = lazy(() => import('@/containers/pdf-view-embed-page'));
const PdfViewIframePage = lazy(() => import('@/containers/pdf-view-iframe-page'));
const PdfViewObjectPage = lazy(() => import('@/containers/pdf-view-object-page'));
const PdfViewPdfjsPage = lazy(() => import('@/containers/pdf-view-pdfjs-page'));
const TableResizablePage = lazy(() => import('@/containers/table-resizable-page'));
const TableDragSortingHandlerPage = lazy(() => import('@/containers/table-drag-sorting-handler-page'));
const TableDragSortingPage = lazy(() => import('@/containers/table-drag-sorting-page'));
const DotPage = lazy(() => import('@/containers/dot-page')); // loading效果
const ModalDraggablePage = lazy(() => import('@/containers/modal-draggable-page')); // 可拖拽弹框
const ChatRobotPage = lazy(() => import('@/containers/chat-robot-page')); // 聊天机器人
const MarkdownPage = lazy(() => import('@/containers/markdown-page')); // markdown渲染
const SlatePage = lazy(() => import('@/containers/slate-page')); // 富文本编辑器
const MdEditorPage = lazy(() => import('@/containers/md-editor-page'));

/**
 * 使页面在没有门户时也可以打开
 * login、register 登录注册页
 * /portal/ 门户下展示
 * /root/ 单独打开页面
 */
const portaItems = [
    {
        path: 'md_editor_page',
        element: (
            <Suspense fallback={<>...</>}>
                <MdEditorPage />
            </Suspense>
        ),
    },
    {
        path: 'home',
        element: <HomePage />,
    },
    {
        path: 'dashboard',
        element: (
            <Suspense fallback={<>...</>}>
                <DashboardPage />
            </Suspense>
        ),
    },
    {
        path: '', // 没有上级时给自身添加RootErrorBoundary
        element: <Outlet />,
        errorElement: <RootErrorBoundary />,
        children: [
            {
                path: 'projects/:projectId',
                element: <Project />,
                errorElement: <ProjectErrorBoundary />,
                loader: projectLoader,
            },
        ],
    },
    {
        path: 'infinite_scroll_bottom',
        element: (
            <Suspense fallback={<>...</>}>
                <InfiniteScrollBottomPage />
            </Suspense>
        ),
    },
    {
        path: 'infinite_scroll_top',
        element: (
            <Suspense fallback={<>...</>}>
                <InfiniteScrollTopPage />
            </Suspense>
        ),
    },
    {
        path: 'virtual_list',
        element: (
            <Suspense fallback={<>...</>}>
                <VirtualListPage />
            </Suspense>
        ),
    },
    {
        path: 'protected',
        element: (
            <Suspense fallback={<>...</>}>
                <ProtectedPage />
            </Suspense>
        ),
    },
    {
        path: 'display',
        element: (
            <Suspense fallback={<>...</>}>
                <DisplayPage />
            </Suspense>
        ),
    },
    {
        path: 'shop',
        element: (
            <Suspense fallback={<>...</>}>
                <ShopPage />
            </Suspense>
        ),
    },
    {
        path: 'game',
        element: (
            <Suspense fallback={<>...</>}>
                <GamePage />
            </Suspense>
        ),
    },
    {
        path: 'live',
        element: (
            <Suspense fallback={<>...</>}>
                <LivePage />
            </Suspense>
        ),
    },
    {
        path: 'pdf_view_embed_page',
        element: (
            <Suspense fallback={<>...</>}>
                <PdfViewEmbedPage />
            </Suspense>
        ),
    },
    {
        path: 'pdf_view_iframe_page',
        element: (
            <Suspense fallback={<>...</>}>
                <PdfViewIframePage />
            </Suspense>
        ),
    },
    {
        path: 'pdf_view_object_page',
        element: (
            <Suspense fallback={<>...</>}>
                <PdfViewObjectPage />
            </Suspense>
        ),
    },
    {
        path: 'pdf_view_pdfjs_page',
        element: (
            <Suspense fallback={<>...</>}>
                <PdfViewPdfjsPage />
            </Suspense>
        ),
    },
    {
        path: 'table_resizable_page',
        element: (
            <Suspense fallback={<>...</>}>
                <TableResizablePage />
            </Suspense>
        ),
    },
    {
        path: 'table_drag_sorting_handler_page',
        element: (
            <Suspense fallback={<>...</>}>
                <TableDragSortingHandlerPage />
            </Suspense>
        ),
    },
    {
        path: 'table_drag_sorting_page',
        element: (
            <Suspense fallback={<>...</>}>
                <TableDragSortingPage />
            </Suspense>
        ),
    },
    {
        path: 'dot_page',
        element: (
            <Suspense fallback={<>...</>}>
                <DotPage />
            </Suspense>
        ),
    },
    {
        path: 'modal_draggable_page',
        element: (
            <Suspense fallback={<>...</>}>
                <ModalDraggablePage />
            </Suspense>
        ),
    },
    {
        path: 'chat_robot_page',
        element: (
            <Suspense fallback={<>...</>}>
                <ChatRobotPage />
            </Suspense>
        ),
    },
    {
        path: 'markdown_page',
        element: (
            <Suspense fallback={<>...</>}>
                <MarkdownPage />
            </Suspense>
        ),
    },
    {
        path: 'slate_page',
        element: (
            <Suspense fallback={<>...</>}>
                <SlatePage />
            </Suspense>
        ),
    },
];

const router = createBrowserRouter([
    {
        path: '/',
        element: <Outlet />,
        errorElement: <RootErrorBoundary />,
        children: [
            {
                index: true,
                element: <LoginPage />,
            },
            {
                path: 'login',
                element: <LoginPage />,
            },
            {
                path: 'register',
                element: (
                    <Suspense fallback={<>...</>}>
                        <RegisterPage />
                    </Suspense>
                ),
            },
        ],
    },
    {
        path: '/portal',
        element: (
            <Suspense fallback={<>...</>}>
                <NavProvider>
                    <RequireAuth>
                        <LayoutPage />
                    </RequireAuth>
                </NavProvider>
            </Suspense>
        ),
        errorElement: <RootErrorBoundary />,
        children: [
            {
                index: true,
                element: <HomePage />,
            },
            ...portaItems,
            {
                path: '*',
                element: (
                    <Suspense fallback={<>...</>}>
                        <NoMatchPage />
                    </Suspense>
                ),
            },
        ],
    },
    {
        path: '/root',
        element: (
            <RequireAuth>
                <Outlet />
            </RequireAuth>
        ),
        errorElement: <RootErrorBoundary />,
        children: [
            {
                index: true,
                element: <HomePage />,
            },
            ...portaItems,
            {
                path: '*',
                element: (
                    <Suspense fallback={<>...</>}>
                        <NoMatchPage />
                    </Suspense>
                ),
            },
        ],
    },
    {
        path: '*',
        element: (
            <Suspense fallback={<>...</>}>
                <NoMatchPage />
            </Suspense>
        ),
    },
]);
if ((import.meta as any).hot) {
    // console.log('hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh');
    (import.meta as any).hot.dispose(() => router.dispose());
}
export default router;
