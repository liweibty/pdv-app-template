import React, { useEffect, useMemo } from 'react';
import Markdown from 'react-markdown';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism';
import remarkGfm from 'remark-gfm';
import rehypeKatex from 'rehype-katex';
import remarkMath from 'remark-math';
import rehypeRaw from 'rehype-raw';
import 'katex/dist/katex.min.css';
import DPlayer from 'dplayer';
import parsePlayer from '@/utils/parsePlayer';
// Did you know you can use tildes instead of backticks for code in markdown? ✨
let markdown = `
# Hi, *Pluto*!
Here is some JavaScript code:
Just a link: www.nasa.gov.
~~~js
console.log('It works!')
~~~

~~~npm
npm i
~~~

A paragraph with *emphasis* and **strong importance**.

> A block quote with ~strikethrough~ and a URL: https://reactjs.org.

* Lists
* [ ] todo
* [x] done

A table:

| a | b |
| - | - |
This ~is not~ strikethrough, but ~~this is~~!
The lift coefficient ($C_L$) is a dimensionless coefficient.
<div class="note">

Some *emphasis* and <strong>strong</strong>!

</div>
some code
[dplayer url="/dplayer.mp4" pic="dplayer.png" autoplay="true" /]
some others code
`;

markdown = parsePlayer(markdown);
const components = {
    code(props: any) {
        const { children, className, node, ...rest } = props;
        const match = /language-(\w+)/.exec(className || '');
        console.log(props, 'props');
        return match ? (
            <SyntaxHighlighter {...rest} PreTag='div' language={match[1]} style={dark}>
                {String(children).replace(/\n$/, '')}
            </SyntaxHighlighter>
        ) : (
            <code {...rest} className={className}>
                {children}
            </code>
        );
    },
};

const defaultConfig = {
    autoplay: false,
    theme: '#FADFA3',
    loop: false,
    lang: 'zh-cn',
    screenshot: true,
    hotkey: true,
    preload: 'auto',
    logo: '/logo192.png',
    volume: 0.7,
    mutex: true,
    video: {
        // url: '/dplayer.mp4',
        // pic: '/dplayer.png',
        thumbnails: '/thumbnails.jpg',
        type: 'auto',
    },
    subtitle: {
        url: '/dplayer.vtt',
        type: 'webvtt',
        fontSize: '20px',
        bottom: '10%',
        color: '#b7daff',
    },
    danmaku: {
        id: '9E2E3368B56CDBB4',
        api: 'https://api.prprpr.me/dplayer/',
        token: 'tokendemo',
        maximum: '1000',
        addition: ['https://api.prprpr.me/dplayer/v3/bilibili?aid=4157142'],
        user: 'DIYgod',
        bottom: '15%',
        unlimited: true,
        speedRate: 0.5,
    },
    contextmenu: [
        {
            text: '用友科技所有',
            link: 'https://github.com/DIYgod/DPlayer',
        },
        {
            text: '查看源码',
            click: (player: any) => {
                console.log(player);
            },
        },
    ],
    highlight: [
        {
            time: 5,
            text: '这是第 5 秒',
        },
        {
            time: 10,
            text: '这是 10 秒',
        },
    ],
};

export default function MarkdownPage() {
    // 视频
    useEffect(() => {
        let dPlayers: any[] = [];

        function load(d: any, conf: any) {
            conf.container = d;
            dPlayers.push(new DPlayer(conf));
        }

        let k = document.getElementsByClassName('dplayer') as any;
        for (let j = 0; j < k.length; j += 1) {
            const { autoplay, url, pic } = JSON.parse(k[j].dataset.config);
            load(k[j], { ...defaultConfig, autoplay, video: { ...defaultConfig.video, url, pic } });
        }

        return () => {
            k = null;
            for (let i = 0; i < dPlayers.length; i += 1) {
                dPlayers[i].destroy();
            }
            dPlayers = [];
        };
    }, []);

    return (
        <div className=' overflow-auto'>
            <Markdown
                remarkPlugins={[remarkGfm, remarkMath]}
                rehypePlugins={[rehypeKatex, rehypeRaw]}
                components={components}
            >
                {markdown}
            </Markdown>
        </div>
    );
}
