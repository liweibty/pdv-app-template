import React from 'react';
import { Switch, Button } from 'antd';
import { BulbOutlined, AlertOutlined } from '@ant-design/icons';
import { useRootConfig } from '@/components/root-config';

// theme设置
export default function SettingDrawer() {
    const { theme, setTheme, setTokenId } = useRootConfig();

    return (
        <div>
            <div className=' mt-4'>
                <div>关灯</div>

                <Switch
                    checked={theme === 'dark'}
                    checkedChildren={<BulbOutlined />}
                    unCheckedChildren={<AlertOutlined />}
                    onChange={(checked) => setTheme(checked ? 'dark' : 'light')}
                    size='default'
                    className=' mt-4'
                />
            </div>
            <div className=' mt-4'>
                <div>按钮颜色</div>
                <div className='flex w-full justify-around mt-4'>
                    <Button
                        shape='circle'
                        style={{
                            background: '#1fb6ff',
                        }}
                        onClick={() => {
                            setTokenId('blue');
                        }}
                    />
                    <Button
                        shape='circle'
                        style={{
                            background: '#7e5bef',
                        }}
                        onClick={() => {
                            setTokenId('purple');
                        }}
                    />
                    <Button
                        shape='circle'
                        style={{
                            background: '#ff49db',
                        }}
                        onClick={() => {
                            setTokenId('pink');
                        }}
                    />
                    <Button
                        shape='circle'
                        style={{
                            background: '#ff7849',
                        }}
                        onClick={() => {
                            setTokenId('orange');
                        }}
                    />
                    <Button
                        shape='circle'
                        style={{
                            background: '#13ce66',
                        }}
                        onClick={() => {
                            setTokenId('green');
                        }}
                    />
                    <Button
                        shape='circle'
                        style={{
                            background: '#ffc82c',
                        }}
                        onClick={() => {
                            setTokenId('yellow');
                        }}
                    />
                </div>
            </div>
        </div>
    );
}
