import React, { useCallback, useEffect, useRef, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
// 列表无限滚动
// 下拉刷新
// 上拉加载更多
function InfiniteScrollPage() {
    const [items, setItems] = useState<any[]>([]);
    const [hasMore, setHasMore] = useState<boolean>(true);

    const containerRef = useRef<HTMLDivElement | null>(null);
    useEffect(() => {
        const mockData = [...new Array(100)].map((_, index) => ({ id: index, name: `item_${index}` }));
        setItems(mockData);
    }, []);

    // 加载更多
    const fetchMoreData = useCallback(function fetchMoreData() {
        // console.log('加载更多');
        setTimeout(() => {
            setItems((preItems) => {
                const mockData = [...new Array(100)].map((_, index) => ({
                    id: preItems.length + index,
                    name: `item_${preItems.length + index}`,
                }));
                return [...preItems, ...mockData];
            });
            setHasMore(false);
        }, 2000);
    }, []);

    // 刷新
    const refresh = useCallback(function refresh() {
        // console.log('刷新');
        setTimeout(() => {
            const mockData = [...new Array(100)].map((_, index) => ({ id: index, name: `item_${index}` }));
            setItems(mockData);
            setHasMore(true);
        }, 2000);
    }, []);

    return (
        <div className='w-full h-full overflow-auto' ref={containerRef}>
            {containerRef.current && (
                <InfiniteScroll
                    dataLength={items.length}
                    next={fetchMoreData}
                    hasMore={hasMore}
                    loader={<div className='h-8 w-full flex items-center justify-center'>加载中...</div>}
                    endMessage={<div className='h-8 w-full flex items-center justify-center'>没有更多数据了</div>}
                    // below props only if you need pull down functionality
                    refreshFunction={refresh}
                    pullDownToRefresh
                    pullDownToRefreshThreshold={50}
                    pullDownToRefreshContent={
                        <div className='h-8 w-full flex items-center justify-center '>&#8595;下拉刷新</div>
                    }
                    releaseToRefreshContent={
                        <div className='h-8 w-full flex items-center justify-center'>&#8593;释放刷新</div>
                    }
                    scrollableTarget={containerRef.current as any}
                >
                    {items.map((item) => (
                        <div className='h-8 w-full flex items-center border-solid border-gray-50  px-4' key={item.id}>
                            div - #{item.name}
                        </div>
                    ))}
                </InfiniteScroll>
            )}
        </div>
    );
}

export default InfiniteScrollPage;
