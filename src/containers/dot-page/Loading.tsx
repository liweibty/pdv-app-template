import React, { memo } from 'react';
import './Loading.less';

function Loading() {
    return (
        <div className='pdv-loading'>
            <div className='pdv-loading__box'>
                <div className='pdv-loading__item'>
                    <span className='pdv-loading__dot pdv-loading__dot-1'> </span>
                    <span className='pdv-loading__dot pdv-loading__dot-2' />
                    <span className='pdv-loading__dot pdv-loading__dot-3' />
                </div>
                <div className='pdv-loading__item'>
                    <span className='pdv-loading__dot pdv-loading__dot-3' />
                    <span className='pdv-loading__dot pdv-loading__dot-1'> </span>
                    <span className='pdv-loading__dot pdv-loading__dot-2' />
                </div>
                <div className='pdv-loading__item'>
                    <span className='pdv-loading__dot pdv-loading__dot-2' />
                    <span className='pdv-loading__dot pdv-loading__dot-3'> </span>
                    <span className='pdv-loading__dot pdv-loading__dot-1' />
                </div>
            </div>
        </div>
    );
}

export default memo(Loading);
