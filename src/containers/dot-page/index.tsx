import React from 'react';
import Loading from './Loading';
import './index.less';

export default function DotPage() {
    return (
        <div>
            <Loading />
            <p className='text'>
                识别中
                <span className='dot'>...</span>
            </p>
        </div>
    );
}
