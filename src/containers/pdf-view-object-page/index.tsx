import React from 'react';
import { Layout } from 'antd';

const url =
    'https://www.lilnong.top/static/pdf/B-4-RxJS%E5%9C%A8React%E4%B8%AD%E7%9A%84%E5%BA%94%E7%94%A8-%E9%BE%99%E9%80%B8%E6%A5%A0_.pdf';

// https://juejin.cn/post/7143088940953075743    //
// pdf预览
function EmbedPdfViewPage() {
    return (
        <Layout className=' w-full h-full overflow-auto'>
            <object type='application/pdf' width='100%' height='100%' data={`${url}#toolbar=0`}>
                pdf
            </object>
        </Layout>
    );
}

export default EmbedPdfViewPage;
