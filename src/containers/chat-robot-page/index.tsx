import React, { useCallback } from 'react';
import { Layout } from 'antd';
import Draggable from 'react-draggable'; // The default

export default function ChatRobotPage() {
    const handleStart = useCallback(function handleStart() {}, []);
    const handleDrag = useCallback(function handleDrag() {}, []);
    const handleStop = useCallback(function handleStop() {}, []);
    return (
        <Layout>
            <Draggable
                axis='both'
                handle='.handle'
                defaultPosition={{ x: 0, y: 0 }}
                position={undefined}
                grid={[25, 25]}
                scale={1}
                onStart={handleStart}
                onDrag={handleDrag}
                onStop={handleStop}
            >
                <div>
                    <div className='handle'>Drag from here</div>
                    <div>This readme is really dragging on...</div>
                </div>
            </Draggable>
        </Layout>
    );
}
