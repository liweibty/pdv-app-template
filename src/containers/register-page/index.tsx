import React, { useCallback } from 'react';
import { Form, Input, Button, Layout } from 'antd';
import { useNavigate, useLocation } from 'react-router-dom';
import { useAuth } from '@/components/auth';

const { Header, Content } = Layout;
/**
 * 注册
 * @returns
 */
function RegisterPage() {
    const navigate = useNavigate();
    const location = useLocation();
    const auth = useAuth();

    const from = location.state?.from?.pathname || '/';

    const handleSubmit = useCallback(
        async function handleSubmit(values: any) {
            auth.saveUserInfo({ id: 1, username: values.values });
            navigate(from, { replace: true });
        },
        [auth, from, navigate],
    );

    return (
        <Layout>
            <Header>新用户注册</Header>
            <Content>
                <Form onFinish={handleSubmit}>
                    <Form.Item label='用户名' name='username'>
                        <Input placeholder='请输入用户名' />
                    </Form.Item>
                    <Form.Item label='密码' name='password'>
                        <Input placeholder='请输入密码' />
                    </Form.Item>
                    <Form.Item label='密码' name='password2'>
                        <Input placeholder='请再次输入密码' />
                    </Form.Item>
                    <Form.Item>
                        <Button type='primary'>注册</Button>
                    </Form.Item>
                </Form>
            </Content>
        </Layout>
    );
}

export default RegisterPage;
