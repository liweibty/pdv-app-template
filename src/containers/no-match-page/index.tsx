import React from 'react';
import { Link } from 'react-router-dom';
import { Layout } from 'antd';

const { Content } = Layout;
// 404页面
function NoMatch() {
    return (
        <Layout className=' '>
            <Content className='flex flex-col items-center mt-10 '>
                <h2>404!Nothing to see here!</h2>
                <p>
                    <Link to='/'>返回主页</Link>
                </p>
            </Content>
        </Layout>
    );
}

export default NoMatch;
