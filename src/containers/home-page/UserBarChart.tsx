import React, { useEffect, useRef } from 'react';
import * as echarts from 'echarts/core';
import { GridComponent } from 'echarts/components';
import { BarChart } from 'echarts/charts';
import { CanvasRenderer } from 'echarts/renderers';

echarts.use([GridComponent, BarChart, CanvasRenderer]);

function UserBarChart() {
    const chartDomRef = useRef<HTMLDivElement | null>(null);

    useEffect(() => {
        const chartDom = chartDomRef.current;
        const myChart = echarts.init(chartDom);
        const option = {
            xAxis: {
                type: 'category',
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            },
            yAxis: {
                type: 'value',
            },
            series: [
                {
                    data: [120, 200, 150, 80, 70, 110, 130],
                    type: 'bar',
                },
            ],
        };

        myChart.setOption(option);
        return () => {
            myChart.dispose();
        };
    }, []);

    return <div ref={chartDomRef} className=' h-96' />;
}

export default UserBarChart;
