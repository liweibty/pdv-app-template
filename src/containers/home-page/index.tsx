import React from 'react';
import { Col, Row, Statistic, Layout, Card } from 'antd';
import { ArrowDownOutlined, ArrowUpOutlined, LikeOutlined } from '@ant-design/icons';
import UserPieChart from './UserPieChart';
import UserBarChart from './UserBarChart';
import UserLineChat from './UserLineChat';
import './index.less';

const { Content } = Layout;
const { Countdown } = Statistic;
const deadline = Date.now() + 1000 * 60 * 60 * 24 * 2 + 1000 * 30; // Dayjs is also OK

function HomePage() {
    return (
        <Layout>
            <Content className=' overflow-auto'>
                <div className=' p-4'>
                    <Row gutter={16}>
                        <Col span={6}>
                            <Card bordered={false} className=' bg-blue-500'>
                                <Statistic
                                    title='Active'
                                    value={11.28}
                                    precision={2}
                                    valueStyle={{
                                        color: '#3f8600',
                                    }}
                                    prefix={<ArrowUpOutlined />}
                                    suffix='%'
                                />
                            </Card>
                        </Col>
                        <Col span={6}>
                            <Card bordered={false} className=' bg-orange-500'>
                                <Statistic
                                    title='Idle'
                                    value={9.3}
                                    precision={2}
                                    valueStyle={{
                                        color: '#cf1322',
                                    }}
                                    prefix={<ArrowDownOutlined />}
                                    suffix='%'
                                />
                            </Card>
                        </Col>
                        <Col span={6}>
                            <Card bordered={false} className=' bg-red-500 '>
                                <Statistic
                                    valueStyle={{
                                        color: 'rgba(255, 255, 255, 0.65)',
                                    }}
                                    title='Feedback'
                                    value={1128}
                                    prefix={<LikeOutlined />}
                                />
                            </Card>
                        </Col>
                        <Col span={6}>
                            <Card bordered={false} className='bg-green-500'>
                                <Countdown
                                    valueStyle={{
                                        color: 'rgba(255, 255, 255, 0.65)',
                                    }}
                                    title='Million Seconds'
                                    value={deadline}
                                    format='HH:mm:ss:SSS'
                                />
                            </Card>
                        </Col>
                    </Row>
                </div>
                <div className=' p-4 '>
                    <Row gutter={16}>
                        <Col span={10}>
                            <Card className='home-page__card'>
                                <UserPieChart />
                            </Card>
                        </Col>
                        <Col span={14}>
                            <Card className='home-page__card'>
                                <UserBarChart />
                            </Card>
                        </Col>
                        <Col span={24} className=' mt-4'>
                            <Card className='home-page__card'>
                                <UserLineChat />
                            </Card>
                        </Col>
                    </Row>
                </div>
            </Content>
        </Layout>
    );
}
export default HomePage;
