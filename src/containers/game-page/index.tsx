import React from 'react';
import { Layout, Carousel, Card, List } from 'antd';
import './index.less';

const { Header, Content } = Layout;

const data = [...new Array(100)].map((_, index) => ({
    id: `id_${index}`,
    title: `Title ${index}`,
}));

function GamePage() {
    return (
        <Layout className=' w-full h-full overflow-auto'>
            <Header>欢迎使用</Header>
            <Carousel autoplay>
                <div>
                    <h3 className='carousel-item'>1</h3>
                </div>
                <div>
                    <h3 className='carousel-item'>2</h3>
                </div>
                <div>
                    <h3 className='carousel-item'>3</h3>
                </div>
                <div>
                    <h3 className='carousel-item'>4</h3>
                </div>
            </Carousel>
            <Content className=' p-4'>
                <List
                    grid={{
                        gutter: 16,
                        xs: 1,
                        sm: 2,
                        md: 4,
                        lg: 4,
                        xl: 6,
                        xxl: 3,
                    }}
                    dataSource={data}
                    renderItem={(item) => (
                        <List.Item>
                            <Card title={item.title}>Card content</Card>
                        </List.Item>
                    )}
                />
            </Content>
        </Layout>
    );
}

export default GamePage;
