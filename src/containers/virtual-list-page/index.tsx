import React, { useEffect, useState, useRef } from 'react';
import List from 'rc-virtual-list';
/**
 * 虚拟列表
 * height number 这里获取了父节点的内容区的高度
 * itemHeight 设置为比实际列表项小的值
 * @returns
 */
function VirtualListPage() {
    const [data, setData] = useState<any[]>([]);
    const [height, setHeight] = useState<number>(0);
    const containerRef = useRef<HTMLDivElement | null>(null);

    useEffect(() => {
        const height = containerRef.current!.clientHeight;
        const style = window.getComputedStyle(containerRef.current!);
        const paddingTop = parseFloat(style.getPropertyValue('padding-top'));
        const paddingBottom = parseFloat(style.getPropertyValue('padding-bottom'));
        const contentHeight = height - paddingTop - paddingBottom;
        setHeight(contentHeight);
    }, []);

    useEffect(() => {
        const mockData = [...new Array(1000)].map((_, index) => ({ id: index + 1 }));
        setData(mockData);
    }, []);

    return (
        <div ref={containerRef} className='w-full h-full py-10' id='div'>
            {height > 0 && (
                <List data={data} height={height} itemHeight={30} itemKey='id'>
                    {(item) => (
                        <div className=' w-full h-10 border-gray-50 border-solid flex items-center px-4' key={item.id}>
                            {item.id}
                        </div>
                    )}
                </List>
            )}
        </div>
    );
}

export default VirtualListPage;
