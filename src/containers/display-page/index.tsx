/**
 * live: false
 */

import { EllipsisOutlined } from '@ant-design/icons';
import dayjs from 'dayjs';
import React, { useState } from 'react';
import {
    Layout,
    Upload,
    Tour,
    Input,
    Form,
    QRCode,
    Button,
    Calendar,
    DatePicker,
    Modal,
    Pagination,
    Popconfirm,
    Select,
    Space,
    Table,
    theme,
    TimePicker,
    Transfer,
    Image,
    InputNumber,
    Divider,
} from 'antd';

const { Option } = Select;
const { RangePicker } = DatePicker;
const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        filters: [
            {
                text: 'filter1',
                value: 'filter1',
            },
        ],
    },
    {
        title: 'Age',
        dataIndex: 'age',
    },
];
const { Content } = Layout;
function DisplayPage() {
    const { token } = theme.useToken();
    const [open, setOpen] = useState(false);
    const [tourOpen, setTourOpen] = useState(false);
    const tourRefs = React.useRef([]);
    const showModal = () => {
        setOpen(true);
    };
    const hideModal = () => {
        setOpen(false);
    };
    const info = () => {
        Modal.info({
            title: 'some info',
            content: 'some info',
        });
    };
    const confirm = () => {
        Modal.confirm({
            title: 'some info',
            content: 'some info',
        });
    };
    const steps = [
        {
            title: 'Upload File',
            description: 'Put your files here.',
            target: () => tourRefs.current[0],
        },
        {
            title: 'Save',
            description: 'Save your changes.',
            target: () => tourRefs.current[1],
        },
        {
            title: 'Other Actions',
            description: 'Click to see other actions.',
            target: () => tourRefs.current[2],
        },
    ];
    const fileList = [
        {
            uid: '-1',
            name: 'image.png',
            status: 'done',
            url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        },
        {
            uid: '-2',
            percent: 50,
            name: 'image.png',
            status: 'uploading',
            url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        },
        {
            uid: '-3',
            name: 'image.png',
            status: 'error',
        },
    ];
    return (
        <Layout className=' p-4'>
            <Content className=' overflow-auto'>
                <Space
                    direction='vertical'
                    size={[0, 16]}
                    style={{
                        width: '100%',
                        paddingTop: 16,
                        borderTop: `1px solid ${token.colorBorder}`,
                    }}
                >
                    <Pagination defaultCurrent={1} total={50} showSizeChanger />
                    <Space wrap>
                        <Select
                            showSearch
                            style={{
                                width: 200,
                            }}
                        >
                            <Option value='jack'>jack</Option>
                            <Option value='lucy'>lucy</Option>
                        </Select>
                        <DatePicker />
                        <TimePicker />
                        <RangePicker />
                    </Space>
                    <Space wrap>
                        <Button type='primary' onClick={showModal}>
                            Show Modal
                        </Button>
                        <Button onClick={info}>Show info</Button>
                        <Button onClick={confirm}>Show confirm</Button>
                        <Popconfirm title='Question?'>
                            <Button href='#'>Click to confirm</Button>
                        </Popconfirm>
                    </Space>
                    <Transfer dataSource={[]} showSearch targetKeys={[]} />
                    <div
                        style={{
                            width: 320,
                            border: `1px solid ${token.colorBorder}`,
                            borderRadius: 8,
                        }}
                    >
                        <Calendar fullscreen={false} value={dayjs()} />
                    </div>
                    <Form
                        name='basic'
                        autoComplete='off'
                        labelCol={{
                            sm: {
                                span: 4,
                            },
                        }}
                        wrapperCol={{
                            span: 6,
                        }}
                    >
                        <Form.Item
                            label='Username'
                            name='username'
                            rules={[
                                {
                                    required: true,
                                },
                            ]}
                        >
                            <Input width={200} />
                        </Form.Item>
                        <Form.Item
                            label='Age'
                            name='age'
                            rules={[
                                {
                                    type: 'number',
                                    min: 0,
                                    max: 99,
                                },
                            ]}
                            initialValue={100}
                        >
                            <InputNumber width={200} />
                        </Form.Item>
                        <Form.Item
                            wrapperCol={{
                                offset: 2,
                                span: 6,
                            }}
                        >
                            <Button type='primary' htmlType='submit'>
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                    <Table dataSource={[]} columns={columns} />
                    <Modal title='Locale Modal' open={open} onCancel={hideModal}>
                        <p>Locale Modal</p>
                    </Modal>
                    <Space wrap size={80}>
                        <QRCode
                            value='https://ant.design/'
                            status='expired'
                            onRefresh={() => {
                                //  console.log('refresh')
                            }}
                        />
                        <Image
                            width={160}
                            src='https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
                        />
                    </Space>

                    <Divider orientation='left'>Tour</Divider>
                    <Button type='primary' onClick={() => setTourOpen(true)}>
                        Begin Tour
                    </Button>

                    <Tour open={tourOpen} steps={steps} onClose={() => setTourOpen(false)} />
                </Space>
            </Content>
        </Layout>
    );
}

export default DisplayPage;
