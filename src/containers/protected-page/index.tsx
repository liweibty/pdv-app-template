import React from 'react';
import { Layout } from 'antd';

const { Content } = Layout;
function ProtectedPage() {
    return (
        <Layout className=''>
            <Content className='flex flex-col items-center mt-10'>已登录，可以查看当前页面内容</Content>
        </Layout>
    );
}

export default ProtectedPage;
