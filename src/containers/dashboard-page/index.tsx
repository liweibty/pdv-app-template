import React from 'react';
import { Layout } from 'antd';
import './index.less';

const { Content } = Layout;

function DashboardPage() {
    return (
        <Layout className=' dashboard-page__layout'>
            <Content className='flex justify-center pt-10'>dashboard</Content>
        </Layout>
    );
}

export default DashboardPage;
