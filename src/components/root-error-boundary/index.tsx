import React, { useRouteError } from 'react-router-dom';
import { Button, Layout } from 'antd';

const { Content } = Layout;
// root错误，react-router-dom提供的错误边界 放在上层，子组件抛出的错误
function RootErrorBoundary() {
    const error = useRouteError() as Error;
    return (
        <Layout className=''>
            <Content className='flex flex-col items-center mt-10 '>
                <h1>哦，出了严重的问题 😩</h1>
                <div>
                    <pre>
                        message:
                        {error.message}
                    </pre>
                    <pre>
                        error:
                        {JSON.stringify(error)}
                    </pre>
                </div>

                <Button
                    type='primary'
                    onClick={() => {
                        window.location.href = '/';
                    }}
                >
                    重新加载
                </Button>
            </Content>
        </Layout>
    );
}

export default RootErrorBoundary;
