import { request } from '../request';

const mock: boolean = true;

export interface EmailLoginDto {
    email: string;
    code: string;
    type: 'email';
}

export interface UserNameLoginDto {
    username: string;
    password: string;
    type: 'username';
}

// 登录
export function login(body: UserNameLoginDto | EmailLoginDto) {
    if (mock) {
        return Promise.resolve({
            code: 0,
            data: {
                access_token: '1234567890',
            },
        });
    }
    return request.post('/api/v1/auth/login', body);
}
