import { request } from '../request';

const mock: boolean = true;

export interface CreateUserDto {
    username: string;
    password: string;
    email: string;
    url: string;
    screenName: string;
    group: string;
}

interface UpdateUserDto extends CreateUserDto {}

// 创建
export function userCreate(body: CreateUserDto) {
    if (mock) {
        return Promise.resolve({
            code: 0,
            data: {
                access_token: '1234567890',
            },
        });
    }
    return request.post('/api/v1/user', body);
}
// 查询列表
export function userFindAll(params: { keyWord: string; page: number; pageSize: number }) {
    if (mock) {
        return Promise.resolve({
            code: 0,
            data: {},
        });
    }
    return request.get('/api/v1/user', params);
}
// 查询
export function userFindOne(uid: number) {
    if (mock) {
        return Promise.resolve({
            code: 0,
            data: {},
        });
    }
    return request.get(`/api/v1/user/${uid}`);
}
// 更新
export function userUpdate(uid: number, body: UpdateUserDto) {
    if (mock) {
        return Promise.resolve({
            code: 0,
            data: {},
        });
    }
    return request.patch(`/api/v1/user/${uid}`, body);
}
// 删除
export function userRemove(uid: number) {
    if (mock) {
        return Promise.resolve({
            code: 0,
            data: {},
        });
    }
    return request.delete(`/api/v1/user/${uid}`);
}

export function userSelfInfo() {
    if (mock) {
        return Promise.resolve({
            code: 0,
            data: {
                name: '张三',
            },
        });
    }
    return request.get('/api/v1/user/self');
}
