import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux'; // redux的Provider
import { RouterProvider } from 'react-router-dom'; // router的Provider
import { store } from '@/store';
import router from '@/router';
import reportWebVitals from './reportWebVitals';
import { AuthProvider } from '@/components/auth'; // 登录权限的Provider
import { RootConfigProvider } from '@/components/root-config'; // 多语，主题，皮肤设置的Provider
import 'antd/dist/reset.css';
import './index.css';

const container = document.getElementById('root')!;
const root = createRoot(container);

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RootConfigProvider>
                <AuthProvider>
                    <RouterProvider router={router} fallbackElement={<p>执行初始数据加载</p>} />
                </AuthProvider>
            </RootConfigProvider>
        </Provider>
    </React.StrictMode>,
);

reportWebVitals();
