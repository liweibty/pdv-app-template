/**
 * 获取菜单
 */
export function getMenu(id: number) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (id !== 1) {
                resolve({
                    code: 1,
                    message: '用户不存在',
                });
            }
            resolve({
                code: 0,
                message: '获取用户信息成功',
                data: [
                    {
                        key: 'dashboard',
                        langID: 'menu.dashboard',
                        label: '仪表盘',
                        icon: 'PieChartOutlined',
                        children: [
                            {
                                key: 'dashboard/analysis',
                                langID: 'menu.dashboard.analysis',
                                label: '分析页',
                            },
                            {
                                key: 'dashboard/monitor',
                                langID: 'menu.dashboard.monitor',
                                label: '监控页',
                            },
                            {
                                key: 'dashboard/workplace',
                                langID: 'menu.dashboard.workplace',
                                label: '工作台',
                            },
                        ],
                    },
                    {
                        key: 'list',
                        label: '列表',
                        langID: 'menu.list',
                        icon: 'ContainerOutlined',
                        children: [
                            {
                                key: 'infinite_scroll_group',
                                langID: 'menu.list.scroll',
                                label: '滚动列表',
                                type: 'group',
                                children: [
                                    {
                                        key: 'infinite_scroll_bottom',
                                        langID: 'menu.list.scroll.forward',
                                        label: '正向滚动',
                                    },
                                    {
                                        key: 'infinite_scroll_top',
                                        langID: 'menu.list.scroll.reverse',
                                        label: '反向滚动',
                                    },
                                ],
                            },
                            {
                                key: 'virtual_list_group',
                                langID: 'menu.list.scroll.virtual',
                                label: '虚拟列表',
                                type: 'group',
                                children: [
                                    {
                                        key: 'virtual_list',
                                        langID: 'menu.list.scroll.virtual',
                                        label: '虚拟列表',
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        key: 'form',
                        langID: 'menu.form',
                        label: '表单页',
                        icon: 'DesktopOutlined',
                        children: [
                            {
                                key: 'display',
                                langID: 'menu.form.basic-form',
                                label: '基础表单',
                            },
                        ],
                    },
                    {
                        key: 'pdf',
                        label: 'pdf预览',
                        icon: 'DesktopOutlined',
                        children: [
                            {
                                key: 'pdf_view_embed_page',
                                label: '使用embed',
                            },
                            {
                                key: 'pdf_view_iframe_page',
                                label: '使用iframe',
                            },
                            {
                                key: 'pdf_view_object_page',
                                label: '使用object',
                            },
                            {
                                key: 'pdf_view_pdfjs_page',
                                label: '使用pdfjs',
                            },
                        ],
                    },
                    {
                        key: 'table',
                        label: '表格',
                        icon: 'DesktopOutlined',
                        children: [
                            {
                                key: 'table_resizable_page',
                                label: '拖动宽度',
                            },
                            {
                                key: 'table_drag_sorting_page',
                                label: '拖动排序',
                            },
                            {
                                key: 'table_drag_sorting_handler_page',
                                label: '拖动句柄排序',
                            },
                        ],
                    },
                    {
                        key: 'modal',
                        label: '弹框',
                        icon: 'DesktopOutlined',
                        children: [
                            {
                                key: 'modal_draggable_page',
                                label: '可拖动弹框',
                            },
                        ],
                    },
                    {
                        key: 'other',
                        label: '其他',
                        icon: 'AppstoreOutlined',
                        children: [
                            {
                                key: 'projects/authorized',
                                label: '项目页',
                            },
                            {
                                key: 'projects/unauthorized',
                                label: '未授权项目',
                            },
                            {
                                key: 'projects/broken',
                                label: '项目奔溃',
                            },
                            {
                                key: '404',
                                label: '404',
                            },
                        ],
                    },
                ],
            });
        }, 100);
    });
}
