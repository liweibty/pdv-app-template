/**
 * mock登录
 * @param {*} param0
 * @returns
 */
export function login({
    username,
    password,
    mobile,
    code,
    remember,
}: {
    username?: string;
    password?: string;
    mobile?: string;
    code?: string;
    remember?: boolean;
}): Promise<{
    code: number;
    message: string;
    data: any;
}> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (username) {
                if (username !== 'admin') {
                    resolve({
                        code: 1,
                        message: '用户不存在',
                        data: null,
                    });
                }
                if (password !== 'admin') {
                    resolve({
                        code: 1,
                        message: '密码错误',
                        data: null,
                    });
                }
            } else if (mobile) {
                if (mobile !== '13000000000') {
                    resolve({
                        code: 1,
                        message: '用户不存在',
                        data: null,
                    });
                }
                if (code !== '666666') {
                    resolve({
                        code: 1,
                        message: '验证码错误',
                        data: null,
                    });
                }
            }

            resolve({
                code: 0,
                message: '登录成功',
                data: {
                    id: 1,
                    username,
                    password,
                },
            });
        }, 100);
    });
}

/**
 * mock注销
 */
export function logout(): Promise<{
    code: number;
    message: string;
    data: any;
}> {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve({
                code: 0,
                message: '注销成功',
                data: null,
            });
        }, 100);
    });
}

/**
 * 获取用户信息成功
 */
export function getUserInfo(id: number): Promise<{
    code: number;
    message: string;
    data: any;
}> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (id !== 1) {
                resolve({
                    code: 1,
                    message: '用户不存在',
                    data: null,
                });
            }
            resolve({
                code: 0,
                message: '获取用户信息成功',
                data: {
                    id: 1,
                    username: 'admin',
                    password: 'admin',
                },
            });
        }, 100);
    });
}
