import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';

import counterSlice from '@/slices/counter-slice';
import appSlice from '@/slices/app-slice';

export const store = configureStore({
    reducer: {
        counter: counterSlice,
        app: appSlice,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
